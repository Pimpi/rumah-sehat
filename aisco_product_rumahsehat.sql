--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 13.3

-- Started on 2022-12-27 22:42:02

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3033 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 74551)
-- Name: auth_role_comp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_role_comp (
    id integer NOT NULL
);


ALTER TABLE public.auth_role_comp OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 74554)
-- Name: auth_role_impl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_role_impl (
    allowedpermissions text,
    name character varying(255),
    id integer NOT NULL
);


ALTER TABLE public.auth_role_impl OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 74560)
-- Name: auth_user_comp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_comp (
    id integer NOT NULL
);


ALTER TABLE public.auth_user_comp OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 74563)
-- Name: auth_user_impl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_impl (
    allowedpermissions character varying(255),
    email character varying(255),
    name character varying(255),
    id integer NOT NULL
);


ALTER TABLE public.auth_user_impl OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 74569)
-- Name: auth_user_passworded; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_passworded (
    password character varying(255),
    id integer NOT NULL,
    user_id integer
);


ALTER TABLE public.auth_user_passworded OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 74572)
-- Name: auth_user_role_comp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_role_comp (
    id integer NOT NULL
);


ALTER TABLE public.auth_user_role_comp OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 74575)
-- Name: auth_user_role_impl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_role_impl (
    id integer NOT NULL,
    authrole integer,
    authuser integer
);


ALTER TABLE public.auth_user_role_impl OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 74578)
-- Name: auth_user_social; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_social (
    socialid character varying(255),
    id integer NOT NULL,
    user_id integer
);


ALTER TABLE public.auth_user_social OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 74581)
-- Name: automaticreport_activityreport_comp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.automaticreport_activityreport_comp (
    id integer NOT NULL
);


ALTER TABLE public.automaticreport_activityreport_comp OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 74584)
-- Name: automaticreport_activityreport_impl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.automaticreport_activityreport_impl (
    asetnetoawaltahun integer NOT NULL,
    bebandankerugianmanajemendanumum integer NOT NULL,
    bebandankerugianpencariandana integer NOT NULL,
    bebandankerugianprogram integer NOT NULL,
    berakhirnyapembatasanwaktu integer NOT NULL,
    jasalayanan integer NOT NULL,
    kerugianaktuarialdankewajibantahunan integer NOT NULL,
    lainlain integer NOT NULL,
    pembatasan character varying(255),
    pemenuhanpembatasanpemerolehanperalatan integer NOT NULL,
    pemenuhanprogrampembatasan integer NOT NULL,
    penghasilaninvestasijangkapanjang integer NOT NULL,
    penghasilaninvestasilain integer NOT NULL,
    penghasilannetoterealisasikandanbelumterealisasikandariijp integer NOT NULL,
    sumbangan integer NOT NULL,
    id integer NOT NULL,
    periodic_id integer
);


ALTER TABLE public.automaticreport_activityreport_impl OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 74587)
-- Name: automaticreport_financialposition_comp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.automaticreport_financialposition_comp (
    id integer NOT NULL
);


ALTER TABLE public.automaticreport_financialposition_comp OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 74590)
-- Name: automaticreport_financialposition_impl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.automaticreport_financialposition_impl (
    asetnetoterikatpermanen integer NOT NULL,
    asetnetoterikattemporer integer NOT NULL,
    asetnetotidakterikat integer NOT NULL,
    asettetap integer NOT NULL,
    investasijangkapanjang integer NOT NULL,
    investasilancar integer NOT NULL,
    kasdansetarakas integer NOT NULL,
    kewajibantahunan integer NOT NULL,
    pendapatanditerimadimukayangdapatdikembalikan integer NOT NULL,
    persediaandanbiayadibayardimuka integer NOT NULL,
    piutangbunga integer NOT NULL,
    piutanglainlain integer NOT NULL,
    propertiinvestasi integer NOT NULL,
    utangdagang integer NOT NULL,
    utangjangkapanjang integer NOT NULL,
    utanglainlain integer NOT NULL,
    utangwesel integer NOT NULL,
    id integer NOT NULL,
    periodic_id integer
);


ALTER TABLE public.automaticreport_financialposition_impl OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 74593)
-- Name: automaticreport_periodic_comp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.automaticreport_periodic_comp (
    id integer NOT NULL
);


ALTER TABLE public.automaticreport_periodic_comp OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 74596)
-- Name: automaticreport_periodic_impl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.automaticreport_periodic_impl (
    isactive boolean NOT NULL,
    name character varying(255),
    id integer NOT NULL
);


ALTER TABLE public.automaticreport_periodic_impl OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 74599)
-- Name: chartofaccount_comp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chartofaccount_comp (
    id integer NOT NULL
);


ALTER TABLE public.chartofaccount_comp OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 74602)
-- Name: chartofaccount_impl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chartofaccount_impl (
    code integer NOT NULL,
    description character varying(255),
    isvisible character varying(255),
    name character varying(255),
    id integer NOT NULL
);


ALTER TABLE public.chartofaccount_impl OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 74904)
-- Name: donation_comp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.donation_comp (
    id integer NOT NULL
);


ALTER TABLE public.donation_comp OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 74909)
-- Name: donation_confirmation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.donation_confirmation (
    proofoftransfer text,
    recieveraccount character varying(255),
    senderaccount character varying(255),
    status character varying(255),
    id integer NOT NULL,
    record_id integer
);


ALTER TABLE public.donation_confirmation OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 74917)
-- Name: donation_impl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.donation_impl (
    amount bigint NOT NULL,
    date character varying(255),
    description character varying(255),
    email character varying(255),
    name character varying(255),
    paymentmethod character varying(255),
    phone character varying(255),
    id integer NOT NULL,
    income_id integer,
    program_idprogram integer
);


ALTER TABLE public.donation_impl OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 74623)
-- Name: financialreport_comp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.financialreport_comp (
    id integer NOT NULL
);


ALTER TABLE public.financialreport_comp OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 74626)
-- Name: financialreport_expense; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.financialreport_expense (
    id integer NOT NULL,
    record_id integer
);


ALTER TABLE public.financialreport_expense OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 74629)
-- Name: financialreport_impl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.financialreport_impl (
    amount bigint NOT NULL,
    datestamp character varying(255),
    description character varying(255),
    id integer NOT NULL,
    coa_id integer,
    program_idprogram integer
);


ALTER TABLE public.financialreport_impl OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 74635)
-- Name: financialreport_income; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.financialreport_income (
    paymentmethod character varying(255),
    id integer NOT NULL,
    record_id integer
);


ALTER TABLE public.financialreport_income OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 74638)
-- Name: program_comp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.program_comp (
    idprogram integer NOT NULL
);


ALTER TABLE public.program_comp OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 74641)
-- Name: program_impl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.program_impl (
    description character varying(255),
    executiondate character varying(255),
    logourl character varying(255),
    name character varying(255),
    partner character varying(255),
    target character varying(255),
    idprogram integer NOT NULL
);


ALTER TABLE public.program_impl OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 74647)
-- Name: program_operational; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.program_operational (
    idprogram integer NOT NULL,
    record_idprogram integer
);


ALTER TABLE public.program_operational OWNER TO postgres;

--
-- TOC entry 3002 (class 0 OID 74551)
-- Dependencies: 202
-- Data for Name: auth_role_comp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_role_comp (id) FROM stdin;
1
2
3
4
5
6
\.


--
-- TOC entry 3003 (class 0 OID 74554)
-- Dependencies: 203
-- Data for Name: auth_role_impl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_role_impl (allowedpermissions, name, id) FROM stdin;
administrator	Administrator	1
	Registered	2
CreateIncome,UpdateIncome,DeleteIncome,CreateExpense,UpdateExpense,DeleteExpense,CreateFinancialReport,UpdateFinancialReport,DeleteFinancialReport	Keuangan	3
CreateProgram,UpdateProgram,DeleteProgram,CreateOperational,UpdateOperational,DeleteOperational	Humas	4
ReadCOD,CreateCOD,UpdateCOD,DeleteCOD	Fundraising	5
CreateIncome,UpdateIncome,DeleteIncome,CreateExpense,UpdateExpense,DeleteExpense,CreateFinancialReport,UpdateFinancialReport,DeleteFinancialReport,CreateArusKas,UpdateArusKas,DeleteArusKas,CreateProgram,UpdateProgram,DeleteProgram,CreateOperational,UpdateOperational,DeleteOperational,ReadCOD,CreateCOD,UpdateCOD,DeleteCOD	Manajer	6
\.


--
-- TOC entry 3004 (class 0 OID 74560)
-- Dependencies: 204
-- Data for Name: auth_user_comp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_comp (id) FROM stdin;
9
823504148
375763986
1195930392
1489676208
950796334
1530688280
1551932158
1929251135
1407875606
693073957
2133956166
487261664
1186222746
342285995
\.


--
-- TOC entry 3005 (class 0 OID 74563)
-- Dependencies: 205
-- Data for Name: auth_user_impl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_impl (allowedpermissions, email, name, id) FROM stdin;
	admin@user.com	admin	9
 	budi@rumahsehat.com	Budi Hartono	823504148
 	ulya@rumahsehat.com	Ulya Kuswandari	1195930392
 	marudi@rumahsehat.com	Marsudi Siregar	950796334
 	hani@rumahsehat.com	Hani Nasyiah	1551932158
 	ikin@rumahsehat.com	Ikin Ramadan	1407875606
 	ghaliyati@rumahsehat.com	Ghaliyati Astuti	2133956166
	jonathantolhas@gmail.com	Tolhas Sianipar	1186222746
\.


--
-- TOC entry 3006 (class 0 OID 74569)
-- Dependencies: 206
-- Data for Name: auth_user_passworded; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_passworded (password, id, user_id) FROM stdin;
349cbccafc082902f6d88098da92b998129d98c079996b96f305705ffddc67baa935e07353a00b6068e6b0f8e1245ee8d499c80ece5232ad938825cb292bce3b	9	9
349cbccafc082902f6d88098da92b998129d98c079996b96f305705ffddc67baa935e07353a00b6068e6b0f8e1245ee8d499c80ece5232ad938825cb292bce3b	375763986	823504148
349cbccafc082902f6d88098da92b998129d98c079996b96f305705ffddc67baa935e07353a00b6068e6b0f8e1245ee8d499c80ece5232ad938825cb292bce3b	1489676208	1195930392
349cbccafc082902f6d88098da92b998129d98c079996b96f305705ffddc67baa935e07353a00b6068e6b0f8e1245ee8d499c80ece5232ad938825cb292bce3b	1530688280	950796334
349cbccafc082902f6d88098da92b998129d98c079996b96f305705ffddc67baa935e07353a00b6068e6b0f8e1245ee8d499c80ece5232ad938825cb292bce3b	1929251135	1551932158
349cbccafc082902f6d88098da92b998129d98c079996b96f305705ffddc67baa935e07353a00b6068e6b0f8e1245ee8d499c80ece5232ad938825cb292bce3b	693073957	1407875606
349cbccafc082902f6d88098da92b998129d98c079996b96f305705ffddc67baa935e07353a00b6068e6b0f8e1245ee8d499c80ece5232ad938825cb292bce3b	487261664	2133956166
\.


--
-- TOC entry 3007 (class 0 OID 74572)
-- Dependencies: 207
-- Data for Name: auth_user_role_comp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_role_comp (id) FROM stdin;
9
573526704
108565532
67334779
2067602913
259712078
1434284007
1388091143
\.


--
-- TOC entry 3008 (class 0 OID 74575)
-- Dependencies: 208
-- Data for Name: auth_user_role_impl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_role_impl (id, authrole, authuser) FROM stdin;
9	1	9
573526704	1	823504148
108565532	3	1195930392
67334779	2	950796334
2067602913	4	1551932158
259712078	5	1407875606
1434284007	6	2133956166
1388091143	2	1186222746
\.


--
-- TOC entry 3009 (class 0 OID 74578)
-- Dependencies: 209
-- Data for Name: auth_user_social; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_social (socialid, id, user_id) FROM stdin;
114117303552609652240	342285995	1186222746
\.


--
-- TOC entry 3010 (class 0 OID 74581)
-- Dependencies: 210
-- Data for Name: automaticreport_activityreport_comp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.automaticreport_activityreport_comp (id) FROM stdin;
1643082669
279209157
565791946
\.


--
-- TOC entry 3011 (class 0 OID 74584)
-- Dependencies: 211
-- Data for Name: automaticreport_activityreport_impl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.automaticreport_activityreport_impl (asetnetoawaltahun, bebandankerugianmanajemendanumum, bebandankerugianpencariandana, bebandankerugianprogram, berakhirnyapembatasanwaktu, jasalayanan, kerugianaktuarialdankewajibantahunan, lainlain, pembatasan, pemenuhanpembatasanpemerolehanperalatan, pemenuhanprogrampembatasan, penghasilaninvestasijangkapanjang, penghasilaninvestasilain, penghasilannetoterealisasikandanbelumterealisasikandariijp, sumbangan, id, periodic_id) FROM stdin;
0	0	0	0	0	0	0	0	TIDAK TERIKAT	0	0	0	0	0	246000000	1643082669	1388119060
0	0	0	0	0	0	0	0	TERIKAT TEMPORER	0	0	0	0	0	0	279209157	1388119060
0	0	0	3000000	0	0	0	0	TERIKAT PERMANEN	0	0	0	0	0	0	565791946	1388119060
\.


--
-- TOC entry 3012 (class 0 OID 74587)
-- Dependencies: 212
-- Data for Name: automaticreport_financialposition_comp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.automaticreport_financialposition_comp (id) FROM stdin;
951387413
\.


--
-- TOC entry 3013 (class 0 OID 74590)
-- Dependencies: 213
-- Data for Name: automaticreport_financialposition_impl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.automaticreport_financialposition_impl (asetnetoterikatpermanen, asetnetoterikattemporer, asetnetotidakterikat, asettetap, investasijangkapanjang, investasilancar, kasdansetarakas, kewajibantahunan, pendapatanditerimadimukayangdapatdikembalikan, persediaandanbiayadibayardimuka, piutangbunga, piutanglainlain, propertiinvestasi, utangdagang, utangjangkapanjang, utanglainlain, utangwesel, id, periodic_id) FROM stdin;
-3000000	0	246000000	0	0	0	246000000	0	0	0	0	0	0	3000000	0	0	0	951387413	1388119060
\.


--
-- TOC entry 3014 (class 0 OID 74593)
-- Dependencies: 214
-- Data for Name: automaticreport_periodic_comp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.automaticreport_periodic_comp (id) FROM stdin;
2084828950
1388119060
762392996
\.


--
-- TOC entry 3015 (class 0 OID 74596)
-- Dependencies: 215
-- Data for Name: automaticreport_periodic_impl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.automaticreport_periodic_impl (isactive, name, id) FROM stdin;
f	2022	2084828950
t	2023	1388119060
f	2021	762392996
\.


--
-- TOC entry 3016 (class 0 OID 74599)
-- Dependencies: 216
-- Data for Name: chartofaccount_comp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.chartofaccount_comp (id) FROM stdin;
10000
11000
11100
11200
12000
12100
12200
19000
40000
41000
41010
41020
42000
42010
42020
43000
43010
43020
43099
44000
49000
60000
61000
62000
63000
63400
64000
65000
65100
65200
65400
66000
66100
66200
66300
67000
68000
69000
\.


--
-- TOC entry 3017 (class 0 OID 74602)
-- Dependencies: 217
-- Data for Name: chartofaccount_impl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.chartofaccount_impl (code, description, isvisible, name, id) FROM stdin;
10000	    	0	Saldo	10000
11000	    	0	Rekening Bank	11000
11100	Saldo yang tersimpan di rekening bank, untuk kebutuhan/pembiayaan khusus, seperti rekening untuk zakat fithrah, hanya untuk fakir miskin saja.	1	Rekening Bank - Khusus	11100
11200	Saldo yang tersimpan di rekening bank, dan memang jelas bebas digunakan untuk institusi sesuai aturan yang berlaku,	1	Rekening Bank - Umum	11200
12000	    	0	Kas	12000
12100	Saldo yang disimpan pengurus, untuk kebutuhan umum seperti saldo kotak amal Jumat	1	Kas - Umum	12100
12200	Saldo yang disimpan pengurus, untuk kebutuhan khusus seperti saldo kas untuk operasional dan konsumsi petugas	1	Kas - Khusus	12200
19000	Saldo lainnya dalam bentuk non cash yang dihargai dalam rupiah.	0	Saldo Lainnya	19000
40000	    	0	Penerimaan	40000
41000	    	0	Donasi Institusi	41000
41010	Donasi dari pemerintah misalnya seperti BLT	1	Donasi Pemerintah	41010
41020	Donasi dari swasta seperti dari dana CSR atau sumbangan karyawan.	1	Donasi Swasta (CSR)	41020
42000	    	0	Donasi Individu	42000
42010	Donasi dari individu yang tercatat sumber dan identitas pengirim	1	Donasi Individu Tercatat	42010
42020	Donasi dari individu yang tidak diketahui sumber nya atau dinyatakan anonymous.	1	Donasi Anonymous	42020
43000	Rekapitulasi segala Penerimaan institusi dari hasil usaha baik usaha internal maupun kerjasama	0	Penerimaan Hasil Usaha	43000
43010	Penerimaan institusi dari kegiatan penjualan barang	1	Hasil penjualan	43010
43020	Penerimaan institusi dari kegiatan upah atau jasa	1	Hasil upah jasa	43020
43099	Penerimaan institusi dari usaha lain-lain atau bonus/komisi	1	Bonus/ Hasil usaha lain-lain	43099
44000	Penerimaan dalam bentuk iuran rutin, seperti iuran kebersihan.	1	Iuran	44000
49000	Penerimaan yang belum bisa dikategorikan.	1	Penerimaan Lainnya	49000
60000	    	0	Pengeluaran	60000
61000	Pembiayaan dalam bentuk menyalurkan kembali donasi yang diterima kepada pihak lain, seperti penyaluran zakat maal kepada para mustahiq.	1	Penyaluran donasi	61000
62000	Pembiayaan untuk gaji dan honor, seperti gaji marbot, gaji keamanan, honor pembicaran, honor ustadz, moderator	1	Gaji dan Honor	62000
63000	Biaya untuk kebutuhan umum seperti administrasi, iuran kebersihan, dan keamanan	0	Biaya Umum	63000
63400	Pembiayaan administrasi seperti biaya pengurusan akta notaris, biaya transfer dll	1	Biaya Administrasi	63400
64000	Pembiayaan sewa ruangan, alat, sewa jasa, termasuk jasa biaya tranportasi, kurir, akomodasi	1	Biaya Sewa dan Jasa	64000
65000	Pengeluaran yang terkait dengan kegiatan atau program institusi	1	Pengeluaran Kegiatan/Program	65000
65100	Pembiayaan untuk publikasi program, kegiatan	1	Biaya Publikasi	65100
65200	Pembiayaan untuk konsumsi kegiatan/program	1	Biaya Konsumsi	65200
65400	Pembiayaan pembeliaan peralatan untuk kegiatan atau untuk kepentingan ins	1	Biaya Peralatan	65400
66000	Segala pembiayaan/ pengeluaran yang rutin terjadi atau tidak terikat program/kegiatan khusus	1	Pengeluaran Umum/Rutin	66000
66100	Pembiayaan bahan habis pakai yang rutin digunakan, seperti printer, sabun, cairan pembersih.	1	Biaya Bahan Habis Pakai	66100
66200	Pembiayaan rutin operasional, seperti listrik, air, internet	1	Biaya Operasional	66200
66300	Pembiayaan terkait perawatan aset institusi, termasuk cat tembok rutin.	1	Biaya Perawatan	66300
67000	Pembiayaan Pengembangan Sumber daya Insitutusi, seperti pelatihan, beasiswa, buku.	1	Pengembangan SDM/ Pelatihan	67000
68000	Pembiayaan renovasi, perbaikan kerusakan bangunan, alat, kendaraan	1	Biaya Renovasi/ Reparasi	68000
69000	Pembiayaan lain yang belum terkategorikan atau belum jelas kategorinya	1	Biaya-biaya Lainnya	69000
\.


--
-- TOC entry 3025 (class 0 OID 74904)
-- Dependencies: 225
-- Data for Name: donation_comp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.donation_comp (id) FROM stdin;
\.


--
-- TOC entry 3026 (class 0 OID 74909)
-- Dependencies: 226
-- Data for Name: donation_confirmation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.donation_confirmation (proofoftransfer, recieveraccount, senderaccount, status, id, record_id) FROM stdin;
\.


--
-- TOC entry 3027 (class 0 OID 74917)
-- Dependencies: 227
-- Data for Name: donation_impl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.donation_impl (amount, date, description, email, name, paymentmethod, phone, id, income_id, program_idprogram) FROM stdin;
\.


--
-- TOC entry 3018 (class 0 OID 74623)
-- Dependencies: 218
-- Data for Name: financialreport_comp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.financialreport_comp (id) FROM stdin;
1
1626715978
1733259609
1005864899
966766953
2079777932
696151394
1524425797
796646105
241539960
2114735300
1974838489
443556873
929529793
13214336
917546928
169904341
938405285
156520618
\.


--
-- TOC entry 3019 (class 0 OID 74626)
-- Dependencies: 219
-- Data for Name: financialreport_expense; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.financialreport_expense (id, record_id) FROM stdin;
1974838489	2114735300
929529793	13214336
169904341	917546928
\.


--
-- TOC entry 3020 (class 0 OID 74629)
-- Dependencies: 220
-- Data for Name: financialreport_impl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) FROM stdin;
15000000	2022-11-16	Sumbangan biaya konsumsi program pengecekan mata Teluk Wondama	1626715978	42000	76635353
200000000	2022-12-22	Bantuan dana pembuatan toilet umum	1005864899	41010	1666260223
5000000	2022-11-30	Bantuan dana pembelian vitamin untuk warga kelurahan Oesapa	2079777932	42020	1580231942
13000000	2022-12-23	Penjualan Merchandise Rumah Sehat	1524425797	43010	76635353
13000000	2022-12-23	Penjualan Merchandise Rumah Sehat	241539960	43010	1580231942
3000000	2022-11-30	Pembelian konsumsi volunteer kabupaten teluk wondama	2114735300	65000	76635353
10000000	2023-01-03	Pengeluaran pembelian konsumsi untuk keluarga Oesapa NTT	443556873	65000	1580231942
7000000	2023-01-03	Pengeluaran pembelian konsumsi untuk keluarga Oesapa NTT	13214336	65000	1580231942
1000000	2023-01-04	Pembelian perlengkapan toilet	917546928	65000	1666260223
310000	2022-09-07	Donasi individu	938405285	42010	\N
\.


--
-- TOC entry 3021 (class 0 OID 74635)
-- Dependencies: 221
-- Data for Name: financialreport_income; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.financialreport_income (paymentmethod, id, record_id) FROM stdin;
Transfer Bank	1733259609	1626715978
Transfer Bank	966766953	1005864899
Transfer Bank	696151394	2079777932
Transfer Bank	796646105	241539960
transfer	156520618	938405285
\.


--
-- TOC entry 3022 (class 0 OID 74638)
-- Dependencies: 222
-- Data for Name: program_comp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.program_comp (idprogram) FROM stdin;
76635353
1666260223
1580231942
1894948411
14183926
853796854
1951433003
\.


--
-- TOC entry 3023 (class 0 OID 74641)
-- Dependencies: 223
-- Data for Name: program_impl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.program_impl (description, executiondate, logourl, name, partner, target, idprogram) FROM stdin;
Melalui program ini, anda dapat berkontribusi untuk menyediakan layanan pengecekan kesehatan mata bagi warga Teluk Wondama	2023-01-31	https://awsimages.detik.net.id/community/media/visual/2021/10/06/warga-di-isiren-papua-2.jpeg	Peduli Kesehatan Mata Kabupaten Teluk Wondama	Puskesmas setempat	Keluarga di Teluk Wondama	76635353
Melalui program ini, anda dapat berkontribusi untuk menyediakan penyuluhan higienisitas dan pengecekan kesehatan bagi warga Manggarai Utara	2023-02-03	https://cdn-2.tstatic.net/jakarta/foto/bank/images/suasana-permukiman-kumuh-rt-005-rw-011-manggarai-utara.jpg	Penyuluhan Higienisitas Warga Manggarai Utara	Puskesmas Manggarai Utara	Warga Manggarai Utara	1666260223
Melalui program ini, anda dapat berkontribusi untuk membantu penyuluhan untuk mendidik keluarga agar terhindar dari stunting	2023-02-03	https://static.republika.co.id/uploads/images/inpicture_slide/033133600-1638457695-830-556.jpg	Penyuluhan Stunting Kelurahan Oesapa NTT	Fakultas Kedokteran Universitas Indonesia	Keluarga Kelurahan Oesapa NTT	1580231942
Pembayaran listrik tahun 2022			Tagihan listrik Tahun 2022			1894948411
Pembiayaan peralatan alat tulis			Pembelian peralatan alat tulis			853796854
\.


--
-- TOC entry 3024 (class 0 OID 74647)
-- Dependencies: 224
-- Data for Name: program_operational; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.program_operational (idprogram, record_idprogram) FROM stdin;
14183926	1894948411
1951433003	853796854
\.


--
-- TOC entry 2793 (class 2606 OID 74651)
-- Name: auth_role_comp auth_role_comp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_role_comp
    ADD CONSTRAINT auth_role_comp_pkey PRIMARY KEY (id);


--
-- TOC entry 2795 (class 2606 OID 74653)
-- Name: auth_role_impl auth_role_impl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_role_impl
    ADD CONSTRAINT auth_role_impl_pkey PRIMARY KEY (id);


--
-- TOC entry 2797 (class 2606 OID 74655)
-- Name: auth_user_comp auth_user_comp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_comp
    ADD CONSTRAINT auth_user_comp_pkey PRIMARY KEY (id);


--
-- TOC entry 2799 (class 2606 OID 74657)
-- Name: auth_user_impl auth_user_impl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_impl
    ADD CONSTRAINT auth_user_impl_pkey PRIMARY KEY (id);


--
-- TOC entry 2801 (class 2606 OID 74659)
-- Name: auth_user_passworded auth_user_passworded_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_passworded
    ADD CONSTRAINT auth_user_passworded_pkey PRIMARY KEY (id);


--
-- TOC entry 2803 (class 2606 OID 74661)
-- Name: auth_user_role_comp auth_user_role_comp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_role_comp
    ADD CONSTRAINT auth_user_role_comp_pkey PRIMARY KEY (id);


--
-- TOC entry 2805 (class 2606 OID 74663)
-- Name: auth_user_role_impl auth_user_role_impl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_role_impl
    ADD CONSTRAINT auth_user_role_impl_pkey PRIMARY KEY (id);


--
-- TOC entry 2807 (class 2606 OID 74665)
-- Name: auth_user_social auth_user_social_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_social
    ADD CONSTRAINT auth_user_social_pkey PRIMARY KEY (id);


--
-- TOC entry 2811 (class 2606 OID 74667)
-- Name: automaticreport_activityreport_comp automaticreport_activityreport_comp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_activityreport_comp
    ADD CONSTRAINT automaticreport_activityreport_comp_pkey PRIMARY KEY (id);


--
-- TOC entry 2813 (class 2606 OID 74669)
-- Name: automaticreport_activityreport_impl automaticreport_activityreport_impl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_activityreport_impl
    ADD CONSTRAINT automaticreport_activityreport_impl_pkey PRIMARY KEY (id);


--
-- TOC entry 2815 (class 2606 OID 74671)
-- Name: automaticreport_financialposition_comp automaticreport_financialposition_comp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_financialposition_comp
    ADD CONSTRAINT automaticreport_financialposition_comp_pkey PRIMARY KEY (id);


--
-- TOC entry 2817 (class 2606 OID 74673)
-- Name: automaticreport_financialposition_impl automaticreport_financialposition_impl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_financialposition_impl
    ADD CONSTRAINT automaticreport_financialposition_impl_pkey PRIMARY KEY (id);


--
-- TOC entry 2819 (class 2606 OID 74675)
-- Name: automaticreport_periodic_comp automaticreport_periodic_comp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_periodic_comp
    ADD CONSTRAINT automaticreport_periodic_comp_pkey PRIMARY KEY (id);


--
-- TOC entry 2821 (class 2606 OID 74677)
-- Name: automaticreport_periodic_impl automaticreport_periodic_impl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_periodic_impl
    ADD CONSTRAINT automaticreport_periodic_impl_pkey PRIMARY KEY (id);


--
-- TOC entry 2823 (class 2606 OID 74679)
-- Name: chartofaccount_comp chartofaccount_comp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chartofaccount_comp
    ADD CONSTRAINT chartofaccount_comp_pkey PRIMARY KEY (id);


--
-- TOC entry 2825 (class 2606 OID 74681)
-- Name: chartofaccount_impl chartofaccount_impl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chartofaccount_impl
    ADD CONSTRAINT chartofaccount_impl_pkey PRIMARY KEY (id);


--
-- TOC entry 2841 (class 2606 OID 74908)
-- Name: donation_comp donation_comp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.donation_comp
    ADD CONSTRAINT donation_comp_pkey PRIMARY KEY (id);


--
-- TOC entry 2843 (class 2606 OID 74916)
-- Name: donation_confirmation donation_confirmation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.donation_confirmation
    ADD CONSTRAINT donation_confirmation_pkey PRIMARY KEY (id);


--
-- TOC entry 2845 (class 2606 OID 74924)
-- Name: donation_impl donation_impl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.donation_impl
    ADD CONSTRAINT donation_impl_pkey PRIMARY KEY (id);


--
-- TOC entry 2827 (class 2606 OID 74689)
-- Name: financialreport_comp financialreport_comp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_comp
    ADD CONSTRAINT financialreport_comp_pkey PRIMARY KEY (id);


--
-- TOC entry 2829 (class 2606 OID 74691)
-- Name: financialreport_expense financialreport_expense_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_expense
    ADD CONSTRAINT financialreport_expense_pkey PRIMARY KEY (id);


--
-- TOC entry 2831 (class 2606 OID 74693)
-- Name: financialreport_impl financialreport_impl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_impl
    ADD CONSTRAINT financialreport_impl_pkey PRIMARY KEY (id);


--
-- TOC entry 2833 (class 2606 OID 74695)
-- Name: financialreport_income financialreport_income_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_income
    ADD CONSTRAINT financialreport_income_pkey PRIMARY KEY (id);


--
-- TOC entry 2835 (class 2606 OID 74697)
-- Name: program_comp program_comp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program_comp
    ADD CONSTRAINT program_comp_pkey PRIMARY KEY (idprogram);


--
-- TOC entry 2837 (class 2606 OID 74699)
-- Name: program_impl program_impl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program_impl
    ADD CONSTRAINT program_impl_pkey PRIMARY KEY (idprogram);


--
-- TOC entry 2839 (class 2606 OID 74701)
-- Name: program_operational program_operational_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program_operational
    ADD CONSTRAINT program_operational_pkey PRIMARY KEY (idprogram);


--
-- TOC entry 2809 (class 2606 OID 74703)
-- Name: auth_user_social uk_a5tmedqsrf0frqeb6dcbakrax; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_social
    ADD CONSTRAINT uk_a5tmedqsrf0frqeb6dcbakrax UNIQUE (socialid);


--
-- TOC entry 2848 (class 2606 OID 74704)
-- Name: auth_user_passworded fk19s1olt8skpbpguobv5ribt6o; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_passworded
    ADD CONSTRAINT fk19s1olt8skpbpguobv5ribt6o FOREIGN KEY (id) REFERENCES public.auth_user_comp(id);


--
-- TOC entry 2850 (class 2606 OID 74709)
-- Name: auth_user_role_impl fk1fdbc1l60nrlf03rubtij4y6a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_role_impl
    ADD CONSTRAINT fk1fdbc1l60nrlf03rubtij4y6a FOREIGN KEY (id) REFERENCES public.auth_user_role_comp(id);


--
-- TOC entry 2851 (class 2606 OID 74714)
-- Name: auth_user_role_impl fk3pokxn1i18kalevuka456mp6p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_role_impl
    ADD CONSTRAINT fk3pokxn1i18kalevuka456mp6p FOREIGN KEY (authuser) REFERENCES public.auth_user_impl(id);


--
-- TOC entry 2857 (class 2606 OID 74719)
-- Name: automaticreport_financialposition_impl fk43ynu6wf42rv5kllyc28mpyyj; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_financialposition_impl
    ADD CONSTRAINT fk43ynu6wf42rv5kllyc28mpyyj FOREIGN KEY (id) REFERENCES public.automaticreport_financialposition_comp(id);


--
-- TOC entry 2868 (class 2606 OID 74724)
-- Name: program_impl fk4d5bk0altx5ilui761k48iq22; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program_impl
    ADD CONSTRAINT fk4d5bk0altx5ilui761k48iq22 FOREIGN KEY (idprogram) REFERENCES public.program_comp(idprogram);


--
-- TOC entry 2859 (class 2606 OID 74729)
-- Name: automaticreport_periodic_impl fk9gp2y6r4jcmy3ry8oi04n9kdv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_periodic_impl
    ADD CONSTRAINT fk9gp2y6r4jcmy3ry8oi04n9kdv FOREIGN KEY (id) REFERENCES public.automaticreport_periodic_comp(id);


--
-- TOC entry 2858 (class 2606 OID 74734)
-- Name: automaticreport_financialposition_impl fk9sxpt3rybvl0mb7mryblwesmx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_financialposition_impl
    ADD CONSTRAINT fk9sxpt3rybvl0mb7mryblwesmx FOREIGN KEY (periodic_id) REFERENCES public.automaticreport_periodic_comp(id);


--
-- TOC entry 2863 (class 2606 OID 74739)
-- Name: financialreport_impl fkbjsvgc403auq9n9j9xt5fe8h9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_impl
    ADD CONSTRAINT fkbjsvgc403auq9n9j9xt5fe8h9 FOREIGN KEY (id) REFERENCES public.financialreport_comp(id);


--
-- TOC entry 2860 (class 2606 OID 74744)
-- Name: chartofaccount_impl fkcj60s5kw2u1t4teov8jbk52ce; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chartofaccount_impl
    ADD CONSTRAINT fkcj60s5kw2u1t4teov8jbk52ce FOREIGN KEY (id) REFERENCES public.chartofaccount_comp(id);


--
-- TOC entry 2875 (class 2606 OID 74945)
-- Name: donation_impl fkcmyc1veutqnw67d48ba2k3hiq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.donation_impl
    ADD CONSTRAINT fkcmyc1veutqnw67d48ba2k3hiq FOREIGN KEY (id) REFERENCES public.donation_comp(id);


--
-- TOC entry 2853 (class 2606 OID 74754)
-- Name: auth_user_social fkeyexhqdg9y496fqp0u9l4l0uc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_social
    ADD CONSTRAINT fkeyexhqdg9y496fqp0u9l4l0uc FOREIGN KEY (user_id) REFERENCES public.auth_user_comp(id);


--
-- TOC entry 2866 (class 2606 OID 74759)
-- Name: financialreport_income fkfyrib08v86hhq951oxxhrlfuk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_income
    ADD CONSTRAINT fkfyrib08v86hhq951oxxhrlfuk FOREIGN KEY (id) REFERENCES public.financialreport_comp(id);


--
-- TOC entry 2846 (class 2606 OID 74764)
-- Name: auth_role_impl fkg93esbm013a0au2sck1jwa1be; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_role_impl
    ADD CONSTRAINT fkg93esbm013a0au2sck1jwa1be FOREIGN KEY (id) REFERENCES public.auth_role_comp(id);


--
-- TOC entry 2855 (class 2606 OID 74769)
-- Name: automaticreport_activityreport_impl fkhxamifkoo5nne7d17svp09p8f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_activityreport_impl
    ADD CONSTRAINT fkhxamifkoo5nne7d17svp09p8f FOREIGN KEY (id) REFERENCES public.automaticreport_activityreport_comp(id);


--
-- TOC entry 2847 (class 2606 OID 74774)
-- Name: auth_user_impl fkj93qld8dfmwxxethtnkbs0p0p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_impl
    ADD CONSTRAINT fkj93qld8dfmwxxethtnkbs0p0p FOREIGN KEY (id) REFERENCES public.auth_user_comp(id);


--
-- TOC entry 2861 (class 2606 OID 74779)
-- Name: financialreport_expense fkjjo0kqk2tctyo6gqxklrpwpa2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_expense
    ADD CONSTRAINT fkjjo0kqk2tctyo6gqxklrpwpa2 FOREIGN KEY (record_id) REFERENCES public.financialreport_comp(id);


--
-- TOC entry 2849 (class 2606 OID 74784)
-- Name: auth_user_passworded fkl3tsngvir2naifbhumm0v6rqd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_passworded
    ADD CONSTRAINT fkl3tsngvir2naifbhumm0v6rqd FOREIGN KEY (user_id) REFERENCES public.auth_user_comp(id);


--
-- TOC entry 2867 (class 2606 OID 74789)
-- Name: financialreport_income fklhpme8vcrsqivqr4j5m3k2h2j; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_income
    ADD CONSTRAINT fklhpme8vcrsqivqr4j5m3k2h2j FOREIGN KEY (record_id) REFERENCES public.financialreport_comp(id);


--
-- TOC entry 2862 (class 2606 OID 74794)
-- Name: financialreport_expense fklpguobmd4m7bc64p5tbsc8466; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_expense
    ADD CONSTRAINT fklpguobmd4m7bc64p5tbsc8466 FOREIGN KEY (id) REFERENCES public.financialreport_comp(id);


--
-- TOC entry 2872 (class 2606 OID 74930)
-- Name: donation_confirmation fklwjdhdkjmv3v1lhqrhp7ip7kt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.donation_confirmation
    ADD CONSTRAINT fklwjdhdkjmv3v1lhqrhp7ip7kt FOREIGN KEY (id) REFERENCES public.donation_comp(id);


--
-- TOC entry 2854 (class 2606 OID 74804)
-- Name: auth_user_social fkoape53p3lif2celki8tiu8fki; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_social
    ADD CONSTRAINT fkoape53p3lif2celki8tiu8fki FOREIGN KEY (id) REFERENCES public.auth_user_comp(id);


--
-- TOC entry 2873 (class 2606 OID 74935)
-- Name: donation_impl fkp0vq00gimjl8qpt4wbmtecbra; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.donation_impl
    ADD CONSTRAINT fkp0vq00gimjl8qpt4wbmtecbra FOREIGN KEY (income_id) REFERENCES public.financialreport_comp(id);


--
-- TOC entry 2864 (class 2606 OID 74814)
-- Name: financialreport_impl fkpfndxeqft9ig7qoa7x0ct6mlh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_impl
    ADD CONSTRAINT fkpfndxeqft9ig7qoa7x0ct6mlh FOREIGN KEY (program_idprogram) REFERENCES public.program_comp(idprogram);


--
-- TOC entry 2869 (class 2606 OID 74819)
-- Name: program_operational fkqn8t96ewhh7xam4iqbfl65gud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program_operational
    ADD CONSTRAINT fkqn8t96ewhh7xam4iqbfl65gud FOREIGN KEY (idprogram) REFERENCES public.program_comp(idprogram);


--
-- TOC entry 2865 (class 2606 OID 74824)
-- Name: financialreport_impl fkqo7nslx2ra3dx5trrde7pb28f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financialreport_impl
    ADD CONSTRAINT fkqo7nslx2ra3dx5trrde7pb28f FOREIGN KEY (coa_id) REFERENCES public.chartofaccount_comp(id);


--
-- TOC entry 2870 (class 2606 OID 74829)
-- Name: program_operational fkr7knjg5fjc12qmm7fopaqrwap; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program_operational
    ADD CONSTRAINT fkr7knjg5fjc12qmm7fopaqrwap FOREIGN KEY (record_idprogram) REFERENCES public.program_comp(idprogram);


--
-- TOC entry 2852 (class 2606 OID 74834)
-- Name: auth_user_role_impl fkrkludg4ww825oy1pal92rhett; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_role_impl
    ADD CONSTRAINT fkrkludg4ww825oy1pal92rhett FOREIGN KEY (authrole) REFERENCES public.auth_role_comp(id);


--
-- TOC entry 2871 (class 2606 OID 74925)
-- Name: donation_confirmation fksnsvoa531cjp0daaklgx0bjxh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.donation_confirmation
    ADD CONSTRAINT fksnsvoa531cjp0daaklgx0bjxh FOREIGN KEY (record_id) REFERENCES public.donation_comp(id);


--
-- TOC entry 2856 (class 2606 OID 74844)
-- Name: automaticreport_activityreport_impl fktas69jv64v5wsqh86gbt0vacd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automaticreport_activityreport_impl
    ADD CONSTRAINT fktas69jv64v5wsqh86gbt0vacd FOREIGN KEY (periodic_id) REFERENCES public.automaticreport_periodic_comp(id);


--
-- TOC entry 2874 (class 2606 OID 74940)
-- Name: donation_impl fky8kujypbtyus61wodkpovj1t; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.donation_impl
    ADD CONSTRAINT fky8kujypbtyus61wodkpovj1t FOREIGN KEY (program_idprogram) REFERENCES public.program_comp(idprogram);


-- Completed on 2022-12-27 22:42:04

--
-- PostgreSQL database dump complete
--

